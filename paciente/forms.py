from django.views.generic import  ListView
from django import forms
from django.shortcuts import render
from .models import Paciente

class PacienteForm(forms.ModelForm):

    class Meta:
        model = Paciente
        fields = [
            'tipo_documento',
            'numero_documento',
            'nombres',
            'apellido_paterno',
            'apellido_materno',
            'fecha_nacimiento',

            ]

        widgets = {
            'tipo_documento': forms.Select(attrs={'class': 'custom-select custom-select-lg mb-1'}),
            'numero_documento':forms.TextInput(attrs={'class':'form-control'}),
            'nombres': forms.TextInput(attrs={'class': 'form-control'}),
            'apellido_paterno': forms.TextInput(attrs={'class': 'form-control'}),
            'apellido_materno': forms.TextInput(attrs={'class': 'form-control'}),
            'fecha_nacimiento':forms.DateInput(attrs={'placeholder': 'dd-MM-yyyy','class': 'form-control'}),

        }

class BuscarPaciente(forms.ModelForm):
    tipdoc = (('1', 'DNI'), ('2', 'Carnet de Extranjeria'), ('3', 'Indocumentado'))
    class Meta:
        model = Paciente
        fields = [
            'tipo_documento',
            'numero_documento',
        ]
        tipdoc = (('1', 'DNI'), ('2', 'Carnet de Extranjeria'), ('3', 'Indocumentado'))
        widgets = {
            'tipo_documento': forms.Select(attrs={'class': 'custom-select custom-select-lg mb-1'}),
            'numero_documento': forms.TextInput(attrs={'class': 'form-control'}),

        }

