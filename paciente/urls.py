from django.contrib import admin
from django.urls import path
from paciente.views import ListarPaciente,BuscarPaciente
from .views import Paciente_new
from . import views
urlpatterns = [
    path('ingresar_paciente',views.Paciente_new,name='paciente_new'),
    path('listar_paciente', ListarPaciente.as_view(), name='listar_paciente'),
   # path('listar_paciente', PacienteListView.as_view(), name='listar_paciente'),
    path('buscar_paciente', views.BuscarPaciente, name='buscar_paciente'),
    path('editar_paciente/<int:pk>', views.Paciente_editar),
    path('eliminar_paciente/<int:pk>', views.Paciente_eliminar),
]
