from django.views.generic import ListView, FormView, CreateView, UpdateView
from django.shortcuts import redirect, get_object_or_404
from django.shortcuts import render
from .models import Paciente
from .forms import PacienteForm, BuscarPaciente
from django.utils import timezone

# Create your views here.


class ListarPaciente(ListView, FormView):
    template_name = 'listar_paciente.html'
    queryset = Paciente.objects.all()
    model = Paciente
    context_object_name = 'paciente'
    form_class = BuscarPaciente
    form = BuscarPaciente

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        form.is_valid()
        enc = False
        tipo_documento = form.cleaned_data.get('tipo_documento')
        numero_documento = form.cleaned_data.get('numero_documento')
        try:
            paciente = Paciente.objects.get(tipo_documento=tipo_documento,numero_documento=numero_documento)
            if paciente != '':
                return render(request, "listar_paciente.html", {'form': form, 'enc': enc, 'paciente': paciente})
        except:
            enc = True
            return render(request, "listar_paciente.html", {'form': form, 'enc': enc})


def Paciente_new(request):
    enc1 = 0
    if request.method == "POST":
        form = PacienteForm(request.POST)
        enc1 = 1
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('listar_paciente')
    else:
        form = PacienteForm()
    return render(request, 'formulario1.html', {'form': form,'enc1':enc1})


def Paciente_editar(request, pk):
    post = get_object_or_404(Paciente, pk=pk)
    if request.method == "POST":
        form = PacienteForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('listar_paciente')  # pk=post.pk
    else:
        form = PacienteForm(instance=post)
    return render(request, 'formulario1.html', {'form': form})


def Paciente_eliminar(request, pk):
    paciente = Paciente.objects.get(id=pk)
    if request.method == 'POST':
        paciente.delete()
        return redirect('listar_paciente')
    return render(request, 'eliminar_paciente.html', {'paciente': paciente})
